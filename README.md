# README #

This application is a simple Breakout game (or just Arkanoid-like).

I've made this game for purely educational purposes. Written in WinForms, c# language.

Link to game files: https://www.dropbox.com/s/n3sv16mnsounsfd/BreakoutGame.rar?dl=0
Just open app file and check it out.